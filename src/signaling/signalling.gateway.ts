import {
  ConnectedSocket,
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from "@nestjs/websockets";
import { Server, Socket } from "socket.io";
import { OfferDto } from "./dto/offer.dto";
import { AnswerDto } from "./dto/answer.dto";
import { CandidateDto } from "./dto/candidate.dto";

@WebSocketGateway({
  cors: {
    origin: "*",
  },
})
export class SignalingGateway {
  @WebSocketServer()
  server: Server;

  @SubscribeMessage("join-room")
  joinRoom(@MessageBody() room: string, @ConnectedSocket() client: Socket) {
    console.log(room);
    client.join(room);
    const clients = this.server.sockets.adapter.rooms.get(room);
    if (clients.size === 2) {
      let peerId: string;
      clients.forEach((id) => {
        if (id !== client.id) {
          peerId = id;
        }
      });
      this.server.to(client.id).emit("initiate-offer", peerId);
    }
  }

  @SubscribeMessage("offer")
  offer(@MessageBody() offer: OfferDto, @ConnectedSocket() client: Socket) {
    const message = {
      type: "offer",
      offer: offer.offer,
      memberId: client.id,
    };
    client.to(offer.memberId).emit("MessageFromPeer", message);
  }

  @SubscribeMessage("answer")
  answer(@MessageBody() answer: AnswerDto, @ConnectedSocket() client: Socket) {
    const message = {
      type: "answer",
      answer: answer.answer,
      memberId: client.id,
    };
    client.to(answer.memberId).emit("MessageFromPeer", message);
  }

  @SubscribeMessage("candidate")
  candidate(
    @MessageBody() candidate: CandidateDto,
    @ConnectedSocket() client: Socket,
  ) {
    const message = {
      type: "candidate",
      candidate: candidate.candidate,
    };
    client.to(candidate.memberId).emit("MessageFromPeer", message);
  }
}

import { Module } from "@nestjs/common";
import { SignalingGateway } from "./signalling.gateway";
@Module({
  imports: [],
  controllers: [],
  providers: [SignalingGateway],
})
export class SignalingModule {}

# How to use


## Setup

Make sure to install the dependencies:

```bash
pnpm install
```

## Development Server

Start the development server on `http://localhost:3001`:

```bash
pnpm run start:debug
```

## Build to static files


```bash
# pnpm
pnpm run build
```

## live back-end

https://webrtc-nest.onrender.com
